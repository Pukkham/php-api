<?php

namespace Controllers;
use Framework\Services\BaseController;
use Database\DB;
use Exception;

class CheckController extends BaseController {
  public $request;

  function __construct($request) {
    $this->request = $request;
  }

  public function checkTest() {
    // $pdo = DB::connect();
    return $this->response(['check' => 'ok', 'param' => $this->request], 200);
  }

  public function checkMore() {
    return $this->response(['sss' => 5555]);
  }

  public function checkFileLoad() {
    $file = './files/F-1563006945.png';

    return $this->download([
      'path' => $file,
    ], 200);
  }

  public function myapiInsert() {
    DB::connect();
    try {
      DB::$pdo->beginTransaction();

      $data = [
        'id' => 3,
        'name' => 'Jesse'
      ];
      $sql = 'INSERT INTO users (id, name) VALUES (:id, :name)';
      $stmt= DB::$pdo->prepare($sql);
      $stmt->execute($data);
      
      DB::$pdo->commit();
      DB::disconnect();
      return $this->response([
        'ok' => true
      ], 200);
    } catch (Exception $e) {
      // roll back the transaction if something failed
      DB::$pdo->rollback();
      DB::disconnect();
      return $this->response([
        'error' => true,
        'message' => $e->getMessage()
      ], 400);
    }
  }
}